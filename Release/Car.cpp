#include "EulersMethod.h"
#include "Car.h"

Car::Car(double _initialPosition, double _initialVelocity, double _timeGap, double _sensitivity)
{
	CS.position = _initialPosition;
	CS.velocity = _initialVelocity;
	CS.acceleration = 0;
	leader = 0;
	timeGap = _timeGap;
	sensitivity = _sensitivity;
}

// computes acceleration based on distance and time gap
double Car::ControllerAcceleration()
{
	return(sensitivity*((leader->CS.position - CS.position) - CS.velocity * timeGap));	
}

void Car::SetLeader(Car * _leader)
{
	leader = _leader;
	CS.acceleration = ControllerAcceleration();
}

double Car::MPH2FPS(double mph)
{
	return(mph * 5280 / 3600);
}

double Car::FPS2MPH(double mph)
{
	return(mph * 3600 / 5280);
}

void Car::ComputeNS(double deltaT)
{
	static int leaderState = 0;
	static double leaderTimer = 0.0;

	if (leader == 0) {		//car is the leader
		switch (leaderState)
		{
			case 0:
				if (CS.velocity < MPH2FPS(60.0)) {
					NS.acceleration = 8.82; }			// leader accelerates to 60 mph
				else {
					NS.acceleration = 0.0;
					leaderState = 1;
					leaderTimer = 0.0; }
				break;
			case 1:
				if (leaderTimer < 10.0) {
					NS.acceleration = 0.0; }			// leader stays at 60 mph for 10 seconds
				else {
					NS.acceleration = -8.82;
					leaderState = 2; }
				leaderTimer += deltaT;
				break;
			case 2:
				if (CS.velocity > 0) {
					NS.acceleration = -8.82; }			// leader decelerates to 0 mph after 10 seconds
				else {
					NS.acceleration = 0.0;
					leaderState = 3; }
				break;
			case 3:
				NS.acceleration = 0.0;					// leader stays at 0 mph
				break;
			default:
				break;
		}
		NS.velocity = CS.velocity + CS.acceleration * deltaT;		// computes next state velocity
		NS.position = CS.position + CS.velocity*deltaT; }			// computes next state position
	else {
		double distance = leader->GetPosition() - CS.position;			// computes distance between leader and current car
		NS.acceleration = ControllerAcceleration();						// computes next state acceleration
		NS.velocity = Eulers::Euler(CS.velocity, CS.acceleration, deltaT);	// computes next state velocity
		NS.position = Eulers::Euler(CS.position, CS.velocity, deltaT); }	// computes next state position
}

void Car::UpdateCS()															// updates all current states with computed next states
{
	CS.acceleration = NS.acceleration;
	CS.velocity = NS.velocity;
	CS.position = NS.position;
}

double Car::GetPosition()
{
	return CS.position;
}

Car::~Car() {
	delete leader;
}
