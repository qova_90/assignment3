#pragma once
#include "Car.h"
#include <fstream>

class Road {

	typedef Car * CarPtr;			//assigns a pointer to a class
	CarPtr* cars;					//declares a car pointer
	int numCars;  //hold number of cars
	std::ofstream file; // Create file object

	/*
	Function: Constructor to set up road structure for our cars
	Parameters: n - number of cars we want to have
	Return value: none
	*/
	
	public:
	Road(int n);

	/*
	Function: updates the status of our cars on the road
	Parameters: deltaT - time increment we want to use for updates
	Return value: none
	*/
	void UpdateRoad(double deltaT);

	/*
	Function: provides vehicle position at each time step
	Parameters: double timestep
	Return value: none
	*/
	void PrintRoad(double t);

	/*
	Function: release memory
	Parameters: none
	Return value: none
	*/
	~Road();
};