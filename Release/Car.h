
#pragma once

struct Variables {
	double position;
	double velocity;
	double acceleration;
};

class Car {
	Variables NS;
	Variables CS;
	double timeGap;
	double sensitivity;
	Car* leader; // pointer to leader

	double MPH2FPS(double mph);
	double FPS2MPH(double mph);

	/*
	 Function: Computes acceleration based on distance and time gap
	 Parameters: accesses class members
	 return value: sets NS acceleration
	*/
	double ControllerAcceleration();

public:
	/*
	Function: creates a car data structure for each car in the simulation
	Parameters:
		initialPosition - initial position of this car
		intialVelocity - intial velocity of this car
		timeGap - time we want to stay behind car in front of this vehicle
		sensitivity - how quickly this car responds to changes in time to vehicle in front
		Return value: pointer to car structure
*/
	Car(double initialPosition, double initialVelocity, double timeGap, double sensitivity);

	/*
	Function: sets the leader vehicle on the road and its driving pattern
	Parameters:
		*leader - pointer to leader data structure
	Return value: none
*/
	void SetLeader(Car* leader);

	/*
	Function: computes the next position of each car
	Parameters:
		*car - pointer to car to update
		deltaT - time increment for next position update
	Return value: none
	*/
	void ComputeNS(double deltaT);

	/*
	Function: updates each cars state from that calculated by the next state method
	Parameters: *car - car to update current state
	Return value: none
	*/
	void UpdateCS();

	/*
	Function: report position of a car
	Parameters: *car - car for which to report position
	Return value: car's position
	*/
	double GetPosition();
	
	/*
	Function: release memory
	Parameters: none
	Return value: none
	*/

	~Car();
};





